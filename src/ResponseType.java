
public enum ResponseType {
    Response_Invalid(0),
    Response_OK(200),
    Response_BadRequest(400),
    Response_Conflict(409),
    Response_ServiceUnavailable(503);
    private final int id;
        ResponseType(int id) { this.id = id; }
        public int getValue() { return id; }
}

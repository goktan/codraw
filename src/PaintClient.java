import java.io.*;
import java.net.Socket;

public class PaintClient implements Runnable{
    private static Integer latestPaintClientID = -1;

    private Socket clientSocket = null;
    private BufferedReader is;
    private BufferedWriter os;
    private Integer clientID;
    private String hostID; //either hostname or ip
    private String userName; //name of the user coming from introduction
    private String clientVersion; //clientVersion of the user coming from introduction
    private boolean introductionOK = false;
    private boolean isLeaveServer = false;

    private PaintClient(Integer clientID) {
        this.clientID = clientID;
    }

    public static PaintClient createPaintClient() {
        latestPaintClientID++;
        return new PaintClient(latestPaintClientID);
    }

    public String getHostID() {
        return hostID;
    }

    public void setClientFromSocket(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        //todo consider to set a timeout to socket
        hostID = clientSocket.getInetAddress().getHostName();
        is = new BufferedReader(new InputStreamReader((clientSocket.getInputStream())));
        os = new BufferedWriter(new OutputStreamWriter((clientSocket.getOutputStream())));
    }
    private void cleanup() {
        try {
            os.close();
            is.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Integer getClientID() {
        return clientID;
    }
    public void writeToClient(Message msg) throws IOException {
        os.write(msg.toString());
        os.flush();
    }

    public void run() {  // Client starts working but still needs introduction message
        String line;
        System.out.println("PaintClient.run" + clientID);
        while (!isLeaveServer) {
            try {
                Response response = new Response();
                char charr[] = new char[1024];
                int readed = is.read(charr);
                line = new String(charr).substring(0,readed);

                Message msg = new Message(line);
                MessageType type = msg.parse();
                Integer canvasID;
                CoDrawSrv engine = CoDrawSrv.getInstance();
                switch (type) {
                    case Intro:
                        System.out.println("Here comes an introduction message");
                        introductionOK = true;
                        this.userName = msg.getUserName();
                        this.clientVersion = msg.getClientVersion();
                        System.out.println("userName = " + userName);
                        System.out.println("clientVersion = " + clientVersion);
                        response.setStatus(ResponseType.Response_OK);
                        response.addData("id", getClientID().toString());
                        break;
                    case ListRoom:
                        System.out.println("Here comes a listroom message");
                        if(!introductionOK) {
                            response.setStatus(ResponseType.Response_BadRequest);
                        } else if(!clientID.equals(msg.getClientID())) {  //this is unlikely but just in case
                            response.setStatus(ResponseType.Response_BadRequest);
                            System.err.println("We got an unexpected client ID request");
                        } else {
                            response.setStatus(ResponseType.Response_OK);
                            response.addData("list", engine.getRoomsList());
                        }
                        break;
                    case JoinRoom:
                        System.out.println("Here comes a joinroom message");
                        if(!introductionOK) {
                            response.setStatus(ResponseType.Response_BadRequest);
                        } else if(!clientID.equals(msg.getClientID())) {
                            response.setStatus(ResponseType.Response_BadRequest);
                            System.err.println("We got an unexpected client ID request");
                        } else {
                            canvasID = msg.getCanvasID();
                            if(engine.joinRoom(canvasID, this))
                                response.setStatus(ResponseType.Response_OK);
                            else
                                response.setStatus(ResponseType.Response_BadRequest);
                        }

                        break;
                    case Action:
                        System.out.println("Here comes an action message");
                        if(!introductionOK) {
                            response.setStatus(ResponseType.Response_BadRequest);
                        } else if(!clientID.equals(msg.getClientID())) {
                            response.setStatus(ResponseType.Response_BadRequest);
                            System.err.println("We got an unexpected client ID request");
                        } else {
                            response.setStatus(ResponseType.Response_OK);
                            canvasID = msg.getCanvasID();
                            Action act = msg.getAction();
                            if(engine.updateRoom(canvasID, act, this))
                                response.setStatus(ResponseType.Response_OK);
                            else
                                response.setStatus(ResponseType.Response_BadRequest);
                        }

                        break;
                    case LeaveRoom:
                        System.out.println("Here comes a leaveroom message");
                        if(!introductionOK) {
                            response.setStatus(ResponseType.Response_BadRequest);
                        } else if(!clientID.equals(msg.getClientID())) {
                            response.setStatus(ResponseType.Response_BadRequest);
                            System.err.println("We got an unexpected client ID request");
                        } else {
                            canvasID = msg.getCanvasID();
                            if(engine.leaveRoom(canvasID, this))
                                response.setStatus(ResponseType.Response_OK);
                            else
                                response.setStatus(ResponseType.Response_BadRequest);
                        }
                        break;
                    case LeaveServer:
                        System.out.println("Here comes a leaveserver message");
                        if(!introductionOK) {
                            response.setStatus(ResponseType.Response_BadRequest);
                        } else if(!clientID.equals(msg.getClientID())) {
                            response.setStatus(ResponseType.Response_BadRequest);
                            System.err.println("We got an unexpected client ID request");
                        } else {
                            engine.leaveAllRooms(this);
                            response.setStatus(ResponseType.Response_OK);
                            isLeaveServer = true;    //we are done with this client
                        }
                        break;
                    default:  case Invalid:
                        System.out.println("Incoming message is Invalid, check client clientVersion");
                        response.setStatus(ResponseType.Response_BadRequest);
                        break;
                }

                os.write(response.toString());
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        cleanup();
    }
}

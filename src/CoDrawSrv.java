import java.net.ServerSocket;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CoDrawSrv {
    //basic singleton implementation noot thread-safe, not the best, still useful
    private static CoDrawSrv instance = null;
    private CoDrawSrv() {
        // Exists only to defeat instantiation.
        roomsLock = new ReentrantLock();
    }
    public static CoDrawSrv getInstance() {
        if(instance == null) {
            instance = new CoDrawSrv();
        }
        return instance;
    }

    private Lock roomsLock;
    private boolean engineRunning = false;

    //rest of class definition
    private ArrayList<PaintRoom> rooms = new ArrayList<PaintRoom>();
    ServerSocket paintServerSocket = null;

    public boolean engine(){
        if(engineRunning) {
            System.err.println("Server engine is already running, you cannot run multiple engines");
            return false;
        }
        engineRunning = true;
        getRooms().add(PaintRoom.createPaintRoom(""));  //add a tmp room until we have client room creation
        try {
            paintServerSocket = new ServerSocket(9999);
            //here comes a new client request
            while(true) {
                PaintClient pc = PaintClient.createPaintClient();  //create next client
                pc.setClientFromSocket(paintServerSocket.accept());
                Thread thread = new Thread(pc);
                thread.start(); //todo keep track of all paintclient threads
                PaintRoom room = PaintRoom.createPaintRoom();
                room.registerClient(pc);
                getRooms().add(room);
            }
        }
        catch (IOException e) {
            e.printStackTrace(); //TODO find better way?
        }
        return true;
    }

    public ArrayList<PaintRoom> getRooms() {
        return rooms;
    }

    public String getRoomsList() {
        String list="";
        for(PaintRoom room : rooms) {
            list+=room.toString()+",";
        }
        //remove last comma from list
        if (list.length() > 0 && list.charAt(list.length() - 1)==',') {
            list = list.substring(0, list.length()-1);
        }
        return list;
    }

    public boolean joinRoom(Integer roomID, PaintClient pc) {
        roomsLock.lock();
        System.out.println("CoDrawSrv.joinRoom to " + roomID);
        for(PaintRoom room : rooms) {
            if(room.getRoomID().equals(roomID)) {
                boolean registerResp = room.registerClient(pc);
                roomsLock.unlock();
                return registerResp;
            }
        }

        PaintRoom pr = PaintRoom.createPaintRoom("");
        pr.registerClient(pc);
        rooms.add(pr);
        roomsLock.unlock();
        return true;
    }

    public boolean leaveRoom(Integer roomID, PaintClient pc) {
        roomsLock.lock();
        for(PaintRoom room : rooms) {
            if(room.getRoomID().equals(roomID)) {
                boolean retval = room.unRegisterClient(pc);
                roomsLock.unlock();
                return retval;
            }
        }
        roomsLock.unlock();
        return false;
    }

    public boolean leaveAllRooms(PaintClient pc) {
        roomsLock.lock();
        for(PaintRoom room : rooms) {
            room.unRegisterClient(pc);
        }
        roomsLock.unlock();
        return true;
    }

    public boolean updateRoom(Integer roomID, Action act, PaintClient pc) {
        boolean retval = false;
        roomsLock.lock();
        for(PaintRoom room : rooms) {
            if(room.getRoomID().equals(roomID)) {
                retval = room.updateCanvas(act, pc);
                dispatchActionToClients(room, act, pc);
            }
        }
        roomsLock.unlock();
        return retval;
    }

    private boolean dispatchActionToClients(PaintRoom room, Action act, PaintClient pc) {
        Message msg = new Message();
        msg.prepareAction(pc.getClientID(), room.getRoomID(), act);
        for(PaintClient client: room.getClientList()) {
            if(!client.getClientID().equals(pc.getClientID())) {
                try {
                    client.writeToClient(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return true;
    }

}

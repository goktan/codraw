
import java.util.LinkedHashSet;
import java.util.Set;

public class PaintRoom {
    int [][] roomCanvas = new int[600][600];
    private String name;
    private Integer roomID;
    private static Integer latestRoomID = -1;

    private Set<PaintClient> clientList = new LinkedHashSet<PaintClient>();

    private PaintRoom(Integer roomID, String name) {
        this.roomID = roomID;
        if(name.isEmpty())
            this.name = "Room"+roomID;
        else
            this.name = name;
    }

    public static PaintRoom createPaintRoom(String name) {
        latestRoomID++;
        return new PaintRoom(latestRoomID, name);
    }
    public static PaintRoom createPaintRoom(){
        latestRoomID++;
        return new PaintRoom(latestRoomID, "room"+latestRoomID);
    }

    public Integer getClientCount() {
        return clientList.size();
    }

    public Set<PaintClient> getClientList() {
        return clientList;
    }

    boolean registerClient(PaintClient pc) {
        System.out.println("Client" + pc.getClientID() + " with hostname " + pc.getHostID()
            + " has been registered to " + name);
        return clientList.add(pc);
    }

    boolean unRegisterClient(PaintClient pc) {
        System.out.println("Client" + pc.getClientID() + " with hostname " + pc.getHostID()
            + " has been unregistered from " + name);
        return clientList.remove(pc);
    }

    synchronized boolean updateCanvas(Action act, PaintClient lastUpdater) {
        //TODO do something to update canvas
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRoomID() {
        return roomID;
    }
    @Override
    public String toString() {
        return name+";"+roomID;
    }
}

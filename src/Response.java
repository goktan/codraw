import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Response {
    private boolean built;
    private ResponseType status;
    private ArrayList<String> parts = null;
    ArrayList<String> dataList = new ArrayList<String>();
    private String line = "";
    private ResponseType type = null;
    public Response(ResponseType status) {
        this.setStatus(status);
    }
    public HashMap<String, String> responseValues;

    public Response() {
        status = ResponseType.Response_OK;
        this.setStatus(status);
    }

    public Response(String line) {
        this.line = line;
        built = true;
    }

    public void addData(String dataName, String dataValue) {
        dataList.add(dataName + ":" + dataValue);
    }

    public ResponseType getStatus() {
        return status;
    }

    public ArrayList<String> getDataList() {
        return dataList;
    }

    private String buildResponse (){
        if(built){
            return line;
        }
        line = "=\n" +
                "\n" +
                "status:" + getStatus().getValue();
        for (int i = 0; i < dataList.size(); i++) {
            line += "\n"+ dataList.get(i);
        }
        line +="\n.\n";
        built = true;
        return line;
    }

    public ResponseType parse() {
        responseValues = new HashMap<String, String>();
        parts = new ArrayList<String>(Arrays.asList(line.split("\n")));
        if(parts.size() < 2) {
            return ResponseType.Response_Invalid;
        }
        if (!parts.get(0).equals("=") && !parts.get(1).equals("") && !parts.get(2).equals("")) {
            //wrong message type
            return ResponseType.Response_Invalid;
        }
        if(!parts.get(parts.size()-1).equals(".")){
            //wrong message type
            return ResponseType.Response_Invalid;
        }
        for (int i=2; i<parts.size()-1; ++i) {
            String command, value;
            String commandPart[];
            commandPart = parts.get(i).split(":");
            if(commandPart.length != 2) {
                return ResponseType.Response_Invalid;
            }

            command = commandPart[0];
            value = commandPart[1];
            if(command.equals("status")) {
                if(value.equals("200")) {
                    status = ResponseType.Response_OK;
                } else if(value.equals("400")) {
                    status = ResponseType.Response_BadRequest;
                } else if(value.equals("409")) {
                    status = ResponseType.Response_Conflict;
                } else if(value.equals("503")) {
                    status = ResponseType.Response_ServiceUnavailable;
                }
            }else{
                responseValues.put(command, value);
            }
        }
        return getStatus();
    }

    @Override
    public String toString(){
        return buildResponse();
    }

    public void setStatus(ResponseType status) {
        this.status = status;
    }

    public HashMap<String, String> getResponseValues() {
        return responseValues;
    }

}

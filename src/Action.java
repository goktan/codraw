
public class Action {
    private String action = null;
    private Integer x = 0;
    private Integer y = 0;
    private Integer color = 0;

    public Action (String action, Integer x, Integer y, Integer color) {
        setAction(action);
        setX(x);
        setY(y);
        setColor(color);
    }
    public Action(){

    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }
}

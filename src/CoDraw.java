import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Queue;

class CoDraw implements ClientActionHandler{
    Socket clientSocket = null;
    BufferedReader is;
    BufferedWriter os;
    int clientId;
    ArrayList<Message> messageQueue = new ArrayList<Message>();
    ArrayList<PaintRoomProxy> roomList;

    public static void main(String[] args) {
        CoDraw client = new CoDraw();

    }

    public CoDraw(){
        try {
            clientSocket = new Socket("localhost", 9999);
            is = new BufferedReader(new InputStreamReader((clientSocket.getInputStream())));
            os = new BufferedWriter(new OutputStreamWriter((clientSocket.getOutputStream())));
            ClientListener clientListener = new ClientListener(is, this);
            Thread clientListenerThread = new Thread(clientListener);
            clientListenerThread.start();
            Message msg = new Message();
            msg.prepareIntro("ertan", "1.1");
            System.out.println("Sending intro");
            sendMessage(msg);
            clientListenerThread.join();
        }
        catch (UnknownHostException e) {
            System.err.println("Trying to connect to unknown host: " + e);
        } catch (IOException e) {
            System.err.println("IOException:  " + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void cleanup(){
        try {
            os.close();
            is.close();
            clientSocket.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void sendMessage(Message m){
        try{
            os.write(m.toString());
            os.flush();
            messageQueue.add(0, m);  // Use as queue
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Override
    public void responseArrived(Response response) {
        Message msg = messageQueue.remove(0);
        System.out.println("status : " + response.getStatus());
        if(response.getStatus() == ResponseType.Response_OK){
            switch (msg.getType()){
                case Intro:
                    clientId = Integer.parseInt(response.getResponseValues().get("id"));
                    System.out.println("Introduction successful, client id = "+clientId);
                    //Intro success, request room list
                    Message m = new Message();
                    m.prepareList(clientId);
                    sendMessage(m);
                    break;
                case ListRoom:
                    roomList = new ArrayList<PaintRoomProxy>();
                    String list = response.getResponseValues().get("list");
                    String[] rooms = list.split(",");
                    for(String room : rooms){
                        String[] parts = room.split(";");
                        roomList.add(new PaintRoomProxy(parts[0], Integer.parseInt(parts[1])));
                    }
                    // Fetched room list, joining to a room for a test
                    System.out.println("Joining room : " + roomList.get(0).getName());
                    Message join = new Message();
                    join.prepareJoin(clientId, roomList.get(0).getId());
                    sendMessage(join);
                    break;
                 case JoinRoom:
                     System.out.println("Joined room! ");
                     // Testing action
                     Message action = new Message();
                     action.prepareAction(clientId, roomList.get(0).getId(), new Action("draw", 10, 10, Color.red.getRGB()));
                     sendMessage(action);
                     System.out.println("Action message sent");
                     break;
                default:
                    System.out.println(response);
                    break;
            }
        }else{
            System.out.println("Not OK response for message: " + msg.getType() + " : " + response);
        }
    }

    @Override
    public void messageArrived(Message message) {
        System.out.println("received message, is this intentional? [" + message + "]");
    }
}
import java.io.BufferedReader;
import java.io.IOException;

public class ClientListener implements Runnable {
    BufferedReader is;
    ClientActionHandler handler;
    public ClientListener(BufferedReader is, ClientActionHandler handler) {
        this.is = is;
        this.handler = handler;
    }
    public void run() {
        while(true) {
            String response = "";
            try {
                response = is.readLine();  // Block until first line
                while (is.ready()){  // Ready all available data
                    response += "\n" + is.readLine();
                }
                Response resp = new Response(response);
                ResponseType type = resp.parse();
                if(type == ResponseType.Response_Invalid){
                    Message message = new Message(response);
                    message.parse();
                    handler.messageArrived(message);
                }else{
                    handler.responseArrived(resp);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

/**
 * Created by ertan on 09/06/14.
 */
public class PaintRoomProxy {
    private String name;
    private int id;

    public PaintRoomProxy(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}

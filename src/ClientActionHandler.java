/**
 * Created by ertan on 09/06/14.
 */
public interface ClientActionHandler {
    public void responseArrived(Response response);
    public void messageArrived(Message message);
}

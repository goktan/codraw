
public enum MessageType {
    Invalid,
    Intro,
    ListRoom,
    JoinRoom,
    Action,
    LeaveRoom,
    LeaveServer
}

import java.util.ArrayList;
import java.util.Arrays;

public class Message {
    private String line = "";
    private MessageType type = null;
    private ArrayList<String> parts = null;
    private Action action;
    private Integer clientID;
    private String userName;
    private String clientVersion;
    private Integer canvasID;

    public  Message (String command) {     //handle the raw line coming from network
        this.line = command;
    }

    public Message ()  {
        this.line = "";
    }

    @Override
    public String toString() {
        return line;
    }

    private boolean buildMessage() {
        if (!line.isEmpty()) line = "";
        line += "=\n\n";
        for(String commandPart:parts) {
            line+=commandPart+"\n";
        }
        line+=".\n";
        return true;
    }

    public boolean prepareIntro(String userName, String clientVersion) {
        this.type = MessageType.Intro;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        setUserName(userName);
        setClientVersion(clientVersion);
        parts.add("name:"+userName);
        parts.add("version:"+clientVersion);
        parts.add("type:"+"intro");
        return buildMessage();
    }

    public boolean prepareList(Integer clientId) {
        this.type = MessageType.ListRoom;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        setClientID(clientId);
        parts.add("id:"+clientId);
        parts.add("type:"+"listroom");
        return buildMessage();
    }

    public boolean prepareJoin(Integer clientId, Integer canvasId) {
        this.type = MessageType.JoinRoom;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        setClientID(clientId);
        setCanvasID(canvasId);
        parts.add("id:"+clientId);
        parts.add("join:"+canvasId);
        parts.add("type:"+"joinroom");
        return buildMessage();
    }

    public boolean prepareAction(Integer clientID, Integer canvasID, Action act) {
        this.type = MessageType.Action;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        this.setAction(act);
        setClientID(clientID);
        setCanvasID(canvasID);
        setAction(act);
        parts.add("id:"+clientID);
        parts.add("canvas:" +canvasID);
        parts.add("type:"+"action");
        parts.add("action:"+act.getAction());
        parts.add("x:"+act.getX());
        parts.add("y:"+act.getY());
        parts.add("color:"+act.getColor());
        return buildMessage();
    }

    public boolean prepareLeaveRoom(Integer clientID, Integer canvasID) {
        this.type = MessageType.LeaveRoom;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        setClientID(clientID);
        setCanvasID(canvasID);
        parts.add("id:"+clientID);
        parts.add("canvas:"+canvasID);
        parts.add("type:"+"leaveroom");
        return buildMessage();
    }

    public boolean prepareLeaveServer(Integer clientID) {
        this.type = MessageType.LeaveServer;
        if(parts == null)
            parts = new ArrayList<String>();
        else
            parts.clear();
        setClientID(clientID);
        parts.add("id:"+clientID);
        parts.add("type:"+"leaveserver");
        return buildMessage();
    }

    public MessageType parse() {
        action = new Action();
        parts = new ArrayList<String>(Arrays.asList(line.split("\n")));
        if(parts.size() < 2) {
            return MessageType.Invalid;
        }
        if (!parts.get(0).equals("=") && !parts.get(1).equals("") && !parts.get(2).equals("")) {
            //wrong message type
            return MessageType.Invalid;
        }
        if(!parts.get(parts.size()-1).equals(".")){
            //wrong message type
            return MessageType.Invalid;
        }
        for (int i=2; i<parts.size()-1; ++i) {
            String command, value;
            String commandPart[];
            commandPart = parts.get(i).split(":");
            if(commandPart.length != 2) {
                return MessageType.Invalid;
            }
            command = commandPart[0];
            value = commandPart[1];

            System.out.println("command = " + command);
            System.out.println("value = " + value);
            if(command.equals("type")) {
                if(value.equals("intro")) {
                    type = MessageType.Intro;
                } else if(value.equals("listroom")) {
                    type = MessageType.ListRoom;
                } else if(value.equals("joinroom")) {
                    type = MessageType.JoinRoom;
                } else if(value.equals("action")) {
                    type = MessageType.Action;
                } else if(value.equals("leaveroom")) {
                    type = MessageType.LeaveRoom;
                } else if(value.equals("leaveserver")) {
                    type = MessageType.LeaveServer;
                } else {
                    type = MessageType.Invalid;
                }
            } else if (command.equals("name")) {
                setUserName(value);
            } else if (command.equals("version")) {
                setClientVersion(value);
            } else if (command.equals("id")) {
                setClientID(Integer.parseInt(value));
            } else if (command.equals("join")) {
                setCanvasID(Integer.parseInt(value));
            } else if (command.equals("action")) {
                action.setAction(value);
            } else if (command.equals("x")) {
                action.setX(Integer.parseInt(value));
            }else if (command.equals("y")) {
                action.setY(Integer.parseInt(value));
            }else if (command.equals("color")) {
                action.setColor(Integer.parseInt(value));
            }else if (command.equals("canvas")){
                setCanvasID(Integer.parseInt(value));
            }
        }

        return type;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Integer getClientID() {
        return clientID;
    }

    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public Integer getCanvasID() {
        return canvasID;
    }

    public void setCanvasID(Integer canvasID) {
        this.canvasID = canvasID;
    }

    public MessageType getType() {
        return type;
    }

}
